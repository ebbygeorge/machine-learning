'''
program to illustrate the lightweight containers in numpy
'''
import numpy as np
import time

# decleration of basic variables
size = 1000000

# creating python lists
l1 = range(size)
l2 = range(size)

# creating numpy lists
a1 = np.arange(size)
a2 = np.arange(size)

start = time.time()

# finding sum in 2 lists
result1 = [(x+y) for x,y in zip(l1,l2)]

print "list took ", (time.time() - start)*1000

start = time.time()

# finding sum in 2 numpy lists
result2 = a1 +a2

print "numpy took ", (time.time() - start)*1000
