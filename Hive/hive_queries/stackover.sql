create table comments (id int, user_id int) comment 'comments details' row format delimited fields terminated by ',' ;
create table posts (id int, posttype int, creationdate timestamp, score int, viewcount int, owneruserid int, title string, answercount int, commentcount int)row format delimited fields terminated by ',';
create table posttypes (id int, name string)row format delimited fields terminated by ',' ;
create table users (id int, reputation int, displayname string, loc string, age int)row format delimited fields terminated by ',' ;

load data local inpath '/home/ebby/Documents/projects/Stack Overflow Dataset/comments.csv' into table comments;
load data local inpath '/home/ebby/Documents/projects/Stack Overflow Dataset/posttypes.csv' into table posttypes;
load data local inpath '/home/ebby/Documents/projects/Stack Overflow Dataset/users.csv' into table users;
load data local inpath '/home/ebby/Documents/projects/Stack Overflow Dataset/posts.csv' into table posts;

A. Find the display name and no. of posts created by the user who has got maximum reputation.

select count(pt.owneruserid) as noOfPosts, maxrep.displayname, pt.owneruserid from posts pt inner join (select id,displayname from users where reputation in(select max(reputation) from users)) maxrep on maxrep.id = pt.id group by pt.owneruserid,maxrep.displayname;

B. Find the average age of users on the Stack Overflow site.

select avg(age) from users;

C. Find the display name of user who posted the oldest post on Stack Overflow (in terms of date).

select displayname from users where id in(select owneruserid from posts where creationdate in(select min(creationdate) from posts));


D. Find the display name and no. of comments done by the user who has got maximum reputation.

select u.displayname, count(*) as comments from comments c inner join (select id, displayname from users where reputation in (select max(reputation) from users)) u on c.user_id = u.id group by u.displayname;

E. Find the display name of user who has created maximum no. of posts on Stack Overflow.
   Find the displayname of user who has commented max. number of times.

select u.displayname from users u join (select owneruserid,count(*) as cnt from posts group by owneruserid sort by cnt desc limit 1) o on o.owneruserid = u.id ; 

select u.displayname, o.cnt from users u join (select user_id, count(*) as cnt from comments group by user_id sort by cnt desc limit 1) o  on o.user_id = u.id ;

F. Find the owner name and id of user whose post has got maximum no. of view counts so far.

select id, displayname from users where id in(select owneruserid from posts where viewcount in(select max(viewcount) from posts));


G. Find the title and owner name of the post which has maximum no. of answer count. 
   Find the title and owner name of post who has got maximum no. of Comment count.	

select o.title, u.displayname from users u join (select title,owneruserid from posts where answercount in(select max(answercount) from posts)) o on o.owneruserid = u.id ;

H.Find the location which has maximum no of Stack Overflow users.	

select loc, count(*) as c from users group by loc order by c desc limit 2;

I.Find the total no. of answers, posts, comments created by Indian users.	

select count(*) from posts where posttype in (select id from posttypes where name = 'Answer') and owneruserid in(select id from users where loc like '%India%');--519
select count(*) from posts p join (select id from users where loc like '%India%') i on i.id = p.owneruserid;  --726
select (*) from comments c join (select id from users where loc like '%India%') i on i.id = c.user_id; --562

