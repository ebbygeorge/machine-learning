splitLen = 1003 
outputFile = 'tweet'
input = open('tweets_tech.csv', 'r').read().split('\n')

ct = 1
for lines in range(0, len(input), splitLen):
    outputData = input[lines:lines+splitLen]
    output = open(outputFile + str(ct) + '.csv', 'w')
    output.write('\n'.join(outputData))
    output.close()
    ct += 1
