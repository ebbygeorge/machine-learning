package censusWorkingHoursNumPair;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.WritableComparable;

public class NumPair implements WritableComparable<NumPair>{
	
	private DoubleWritable first;
	private IntWritable second;
	private void set(DoubleWritable first, IntWritable second) {
		this.first = first;
		this.second = second;
		
	}
	
	public NumPair()
	{
		set(new DoubleWritable(), new IntWritable());
	}
	public NumPair(Double first, Integer second)
	{
		set(new DoubleWritable(first), new IntWritable(second));
	}
	public NumPair(DoubleWritable first, IntWritable second)
	{
		set(first, second);
	}

	public DoubleWritable getFirst() {
		return first;
	}

	public IntWritable getSecond() {
		return second;
	}

	@Override
	public void readFields(DataInput in) throws IOException {
		// TODO Auto-generated method stub
		first.readFields(in);
		second.readFields(in);
	}

	@Override
	public void write(DataOutput out) throws IOException {
		// TODO Auto-generated method stub
		first.write(out);
		second.write(out);
		
	}

	@Override
	public int compareTo(NumPair numpair) {
		// TODO Auto-generated method stub
		int cmp = first.compareTo(numpair.first);
		if (cmp != 0)
		{
			return cmp;
		}
				
		return second.compareTo(numpair.second);
	}
	@Override
	public int hashCode()
	{
		return first.hashCode()*163 + second.hashCode();
				
	}
	@Override
	public boolean equals(Object o)
	{
		if(o instanceof NumPair)
		{
			NumPair numpair = (NumPair) o;
			return first.equals(numpair.first) && second.equals(numpair.second);
		}
		return  false;
	}
	@Override
	public String toString()
	{
		return first + "\t" + second; 
	}
	

}
