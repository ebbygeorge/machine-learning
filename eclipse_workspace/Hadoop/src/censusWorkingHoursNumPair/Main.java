package censusWorkingHoursNumPair;


import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;



public class Main extends Configured implements Tool  {
	@Override
	public int run(String[] args) throws Exception {
		// TODO Auto-generated method stub
		Configuration conf = this.getConf();
		Job job = Job.getInstance(conf);
		job.setJobName("average");
		job.setJarByClass(Main.class);
		
		job.setMapOutputValueClass(NumPair.class);	
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(DoubleWritable.class);
		
		job.setMapperClass(Map.class);
		job.setReducerClass(Reduce.class);
		job.setCombinerClass(Combine.class);
		
		Path inputFilePath = new Path(args[0]);
//		Path inputFilePath = new Path("/home/ebby/Documents/eclipse_workspace/Hadoop/data/input/census.txt");
		Path outputFilePath = new Path(args[1]);
//		Path outputFilePath = new Path("/home/ebby/Downloads");
		
		FileInputFormat.addInputPath(job, inputFilePath);
		FileOutputFormat.setOutputPath(job, outputFilePath);
		
		
		
		return job.waitForCompletion(true) ? 0 :  1;
	}
	public static void main(String[] args) throws Exception
	{
		int exitcode = ToolRunner.run(new Main(), args);
		System.exit(exitcode); 
	}

}
