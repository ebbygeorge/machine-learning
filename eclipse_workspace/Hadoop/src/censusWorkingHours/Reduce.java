package censusWorkingHours;

import java.io.IOException;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.IntWritable;

import org.apache.hadoop.io.Text;

import org.apache.hadoop.mapreduce.Reducer;


public class Reduce extends Reducer<Text, DoubleWritable, Text, DoubleWritable> {
	@Override
	public void reduce(Text key,Iterable<DoubleWritable> values,Context context) throws IOException, InterruptedException
	{
	int count = 0;
	double sum =0.0;
	for(DoubleWritable value:values)
	{
		sum += value.get();
		count++;
	}
	double ratio =sum/count;
	context.write(key, new DoubleWritable(ratio));
	}
}
